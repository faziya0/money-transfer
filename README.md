# Money-transfer Application

### Swagger url - http://localhost:8088/api/money-transfer/swagger-ui/index.html


### Endpoints


#### Create User

````
  
POST /api/money-transfer/users 
Host: localhost:8088
Content-Type: application/json
{
  "name": "string",
  "surname": "string",
  "password": "string",
  "pin": "string",
  "email": "string"
}
````

#### Authentication 

This API uses JWT (JSON Web Token) Token for authentication. Each request to the protected endpoints must include a valid JWT token in the Authorization header.

#### Obtaining a JWT Token

To obtain a JWT token, you must authenticate with your credentials (pin and password) at the `/auth` endpoint. This will return a JWT token that you can use to access the protected endpoints.

````
  
POST /api/money-transfer/auth
Host: localhost:8088
Content-Type: application/json
{
  "password": "string",
  "pin": "string"
}

Response 

{
    "token": "your_jwt_token"
}

````

#### Using the JWT Token
Once you have obtained a JWT token, include it in the Authorization header of your requests to protected endpoints as follows:

GET /protected/resource

Authorization: Bearer your_jwt_token

#### Testing with Swagger UI

If you are testing the API using Swagger UI, you can include the JWT token in header using the Authorize button

After these the token will be automatically included in the Authorization header for all requests made through the Swagger UI

#### Create Account

````
  
POST /api/money-transfer/accounts
Host: localhost:8088
Content-Type: application/json
Authorization: Bearer token
{
  "accountType": "string"
}

````

#### Transfer API
````
  
POST /api/money-transfer/transfers
Host: localhost:8088
Content-Type: application/json
Authorization: Bearer token

{
  "amount": 0,
  "receiverAccountIban": "string",
  "senderAccountIban": "string",
  "senderMessage": "string"
}

````