package az.sinam.moneytransfer.service;

import az.sinam.moneytransfer.dto.request.UserRequest;
import az.sinam.moneytransfer.dto.response.UserResponse;
import az.sinam.moneytransfer.entity.User;


public interface UserService {

    UserResponse createUser(UserRequest userRequest);
    User findUserByPin(String pin);

}
