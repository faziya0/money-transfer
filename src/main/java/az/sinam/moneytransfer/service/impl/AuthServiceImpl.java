package az.sinam.moneytransfer.service.impl;

import az.sinam.moneytransfer.dto.request.Credential;
import az.sinam.moneytransfer.dto.response.AuthResponse;
import az.sinam.moneytransfer.dto.response.UserResponse;
import az.sinam.moneytransfer.entity.User;
import az.sinam.moneytransfer.exception.UnAuthorizeException;
import az.sinam.moneytransfer.repository.UserRepository;
import az.sinam.moneytransfer.service.AuthService;
import az.sinam.moneytransfer.token.JwtUtil;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import static az.sinam.moneytransfer.constant.Constants.INVALID_LOGIN;

import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final JwtUtil jwtUtil;
    private final ModelMapper modelMapper;

    public AuthServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepository, JwtUtil jwtUtil, ModelMapper modelMapper) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.jwtUtil = jwtUtil;
        this.modelMapper = modelMapper;
    }

    @Override
    public AuthResponse auth(Credential credential) {
        Optional<User> userDB = userRepository.findByPin(credential.getPin());

        if (userDB.isEmpty() || !(passwordEncoder.matches(credential.getPassword(), userDB.get().getPassword()))) {
            throw new UnAuthorizeException(INVALID_LOGIN);
        }

        String token = jwtUtil.createToken(userDB.get());
        UserResponse userResponse = modelMapper.map(userDB.get(), UserResponse.class);

        return new AuthResponse(token, userResponse);
    }

}
