package az.sinam.moneytransfer.service.impl;

import az.sinam.moneytransfer.dto.request.UserRequest;
import az.sinam.moneytransfer.dto.response.UserResponse;
import az.sinam.moneytransfer.entity.User;
import az.sinam.moneytransfer.exception.NotFoundException;
import az.sinam.moneytransfer.repository.UserRepository;
import az.sinam.moneytransfer.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static az.sinam.moneytransfer.constant.Constants.USER_NOT_FOUND;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserResponse createUser(UserRequest userRequest) {
        String encodedPassword = passwordEncoder.encode(userRequest.getPassword());
        User user = modelMapper.map(userRequest, User.class);
        user.setPassword(encodedPassword);
        userRepository.save(user);
        return modelMapper.map(user, UserResponse.class);
    }

    @Override
    public User findUserByPin(String pin) {
        return userRepository.findByPin(pin)
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND));
    }

}
