package az.sinam.moneytransfer.service.impl;

import az.sinam.moneytransfer.dto.request.AccountRequest;
import az.sinam.moneytransfer.dto.request.TransferRequest;
import az.sinam.moneytransfer.dto.response.AccountResponse;
import az.sinam.moneytransfer.entity.Account;
import az.sinam.moneytransfer.entity.User;
import az.sinam.moneytransfer.exception.GeneralException;
import az.sinam.moneytransfer.exception.NotFoundException;
import az.sinam.moneytransfer.repository.AccountRepository;
import az.sinam.moneytransfer.service.AccountService;
import az.sinam.moneytransfer.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static az.sinam.moneytransfer.constant.Constants.ACCOUNT_NOT_ACTIVATE;
import static az.sinam.moneytransfer.constant.Constants.ACCOUNT_NOT_EXIST;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final ModelMapper modelMapper;
    private final UserService userService;

    public AccountServiceImpl(AccountRepository accountRepository, ModelMapper modelMapper, UserService userService) {
        this.accountRepository = accountRepository;
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    @Override
    public List<AccountResponse> getAllAccount() {
        String pin = SecurityContextHolder.getContext().getAuthentication().getName();

        return userService.findUserByPin(pin).getAccounts().stream().map(account -> modelMapper.map(account, AccountResponse.class)).collect(Collectors.toList());

    }

    @Override
    public AccountResponse getAccountByIban(String iban) {
        Account accountByIban = findAccountByIban(iban);
        return modelMapper.map(accountByIban, AccountResponse.class);
    }

    @Override
    public AccountResponse createAccount(AccountRequest accountRequest) {
        String pin = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.findUserByPin(pin);
        Account account = new Account();
        account.setAccountType(accountRequest.getAccountType());
        account.setUser(user);
        accountRepository.save(account);
        return modelMapper.map(account, AccountResponse.class);
    }

    @Override
    public void deleteAccount(String iban) {
        accountRepository.delete(findAccountByIban(iban));
    }

    @Override
    public AccountResponse activateAccount(String iban) {
        Account account = findAccountByIban(iban);
        if (!account.isStatus()) {
            account.setStatus(Boolean.TRUE);
            accountRepository.save(account);
        }
        return modelMapper.map(account, AccountResponse.class);

    }

    @Override
    public List<AccountResponse> getAllActiveAccountByUserPin(String pin) {
        User user = userService.findUserByPin(pin);

        return accountRepository.findByUserAndStatusIsTrue(user).stream().
                map(account -> modelMapper.map(account, AccountResponse.class)).collect(Collectors.toList());

    }

    @Override
    public Account findAccountByIban(String iban) {
        return accountRepository.findByIban(iban).orElseThrow(() -> new NotFoundException(ACCOUNT_NOT_EXIST));
    }

    @Override
    public Account findAccountWhichTransferPossible(String iban) {
        return accountRepository.findByIbanAndStatusIsTrue(iban)
                .orElseThrow(() -> new GeneralException(ACCOUNT_NOT_ACTIVATE));
    }

    @Override
    public void update(Account account) {
        accountRepository.save(account);
    }

    public boolean isAccountOwner(String iban) {
        String pin = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = findAccountByIban(iban).getUser();
        return user.getPin().equalsIgnoreCase(pin);
    }

    public boolean isAccountOwner(TransferRequest transferRequest) {
        String pin = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = findAccountByIban(transferRequest.getSenderAccountIban()).getUser();
        return user.getPin().equalsIgnoreCase(pin);
    }

}
