package az.sinam.moneytransfer.service.impl;

import az.sinam.moneytransfer.dto.request.TransferRequest;
import az.sinam.moneytransfer.dto.response.TransferResponse;
import az.sinam.moneytransfer.entity.Account;
import az.sinam.moneytransfer.entity.Transfer;
import az.sinam.moneytransfer.exception.GeneralException;
import az.sinam.moneytransfer.repository.TransferRepository;
import az.sinam.moneytransfer.service.AccountService;
import az.sinam.moneytransfer.service.TransferService;
import az.sinam.moneytransfer.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static az.sinam.moneytransfer.constant.Constants.NOT_ENOUGH_MONEY;
import static az.sinam.moneytransfer.constant.Constants.SAME_ACCOUNT_NUMBER;

@Service
public class TransferServiceImpl implements TransferService {


    private final AccountService accountService;
    private final ModelMapper modelMapper;
    private final TransferRepository transferRepository;
    private final UserService userService;

    public TransferServiceImpl(AccountService accountService, ModelMapper modelMapper, TransferRepository transferRepository, UserService userService) {
        this.accountService = accountService;
        this.modelMapper = modelMapper;
        this.transferRepository = transferRepository;
        this.userService = userService;
    }

    @Override
    @Transactional
    public TransferResponse transfer(TransferRequest transferRequest) {
        Account receiverAccount = accountService.findAccountWhichTransferPossible(transferRequest.getReceiverAccountIban());
        Account senderAccount = accountService.findAccountWhichTransferPossible(transferRequest.getSenderAccountIban());
        if (senderAccount.getBalance().compareTo(transferRequest.getAmount()) < 0) {
            throw new GeneralException(NOT_ENOUGH_MONEY);
        }

        if (transferRequest.getSenderAccountIban().equalsIgnoreCase(transferRequest.getReceiverAccountIban())) {
            throw new GeneralException(SAME_ACCOUNT_NUMBER);
        }

        BigDecimal amount = transferRequest.getAmount();
        BigDecimal senderBalance = senderAccount.getBalance().subtract(amount);
        BigDecimal receiverBalance = receiverAccount.getBalance().add(amount);
        senderAccount.setBalance(senderBalance);
        accountService.update(senderAccount);
        receiverAccount.setBalance(receiverBalance);
        accountService.update(receiverAccount);

        Transfer transfer = modelMapper.map(transferRequest, Transfer.class);
        transfer.setSenderAccount(senderAccount);
        transfer.setReceiverAccount(receiverAccount);
        Transfer saved = transferRepository.save(transfer);
        return new TransferResponse(saved.getId(), saved.getAmount(), saved.getSenderMessage(), saved.getSenderAccount().getIban(), saved
                .getReceiverAccount().getIban());
    }

    @Override
    @Transactional
    public List<TransferResponse> getAllTransfer() {
        String pin = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Account> accounts = userService.findUserByPin(pin).getAccounts();

        return transferRepository.findByReceiverAccountInOrSenderAccountIn(accounts).stream().map(transfer -> new TransferResponse(transfer.getId(), transfer.getAmount(), transfer.getSenderMessage(), transfer.getSenderAccount().getIban(), transfer
                .getReceiverAccount().getIban())).collect(Collectors.toList());

    }

    @Override
    public List<TransferResponse> getAllTransferFromSender() {
        String pin = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Account> accounts = userService.findUserByPin(pin).getAccounts();

        return transferRepository.findBySenderAccountIn(accounts).stream().map(transfer -> new TransferResponse(transfer.getId(), transfer.getAmount(), transfer.getSenderMessage(), transfer.getSenderAccount().getIban(), transfer
                .getReceiverAccount().getIban())).collect(Collectors.toList());
    }

    @Override
    public List<TransferResponse> getAllTransferFromReceiver() {
        String pin = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Account> accounts = userService.findUserByPin(pin).getAccounts();

        return transferRepository.findByReceiverAccountIn(accounts).stream().map(transfer -> new TransferResponse(transfer.getId(), transfer.getAmount(), transfer.getSenderMessage(), transfer.getSenderAccount().getIban(), transfer
                .getReceiverAccount().getIban())).collect(Collectors.toList());
    }


}
