package az.sinam.moneytransfer.service;

import az.sinam.moneytransfer.dto.request.AccountRequest;
import az.sinam.moneytransfer.dto.response.AccountResponse;
import az.sinam.moneytransfer.entity.Account;


import java.util.List;


public interface AccountService {

    List<AccountResponse> getAllAccount();

    AccountResponse createAccount(AccountRequest accountRequest);

    void deleteAccount(String iban);

    AccountResponse activateAccount(String iban);

    List<AccountResponse> getAllActiveAccountByUserPin(String pin);

    Account findAccountByIban(String iban);

    Account findAccountWhichTransferPossible(String iban);

    void update(Account account);

    AccountResponse getAccountByIban(String iban);

}
