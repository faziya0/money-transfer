package az.sinam.moneytransfer.service;

import az.sinam.moneytransfer.dto.request.TransferRequest;
import az.sinam.moneytransfer.dto.response.TransferResponse;

import java.util.List;

public interface TransferService {

    TransferResponse transfer(TransferRequest transferRequest);

    List<TransferResponse> getAllTransfer();

    List<TransferResponse> getAllTransferFromSender();

    List<TransferResponse> getAllTransferFromReceiver();

}
