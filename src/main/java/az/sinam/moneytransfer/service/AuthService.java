package az.sinam.moneytransfer.service;

import az.sinam.moneytransfer.dto.request.Credential;
import az.sinam.moneytransfer.dto.response.AuthResponse;

public interface AuthService {
    AuthResponse auth(Credential credential);
}
