package az.sinam.moneytransfer.constant;

public class Constants {

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    public static final String ACCOUNT_NOT_EXIST = "account does not exist";
    public static final String USER_NOT_FOUND = "user not found";
    public static final String ACCOUNT_NOT_ACTIVATE = "Account is inactive or does not exist";
    public static final String NOT_ENOUGH_MONEY = "There is no enough money";
    public static final String SAME_ACCOUNT_NUMBER = "Account number is same";
    public static final String ARGUMENT_VALIDATION_FAILED = "Argument validation failed";
    public static final String INVALID_LOGIN = "invalid login or password";

}
