package az.sinam.moneytransfer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnAuthorizeException extends GeneralException {

    public UnAuthorizeException(String message) {
        super(message);
    }

}