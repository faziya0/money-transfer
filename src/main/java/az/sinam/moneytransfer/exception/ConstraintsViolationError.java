package az.sinam.moneytransfer.exception;

public class ConstraintsViolationError {
    public ConstraintsViolationError(String property,String message) {
        this.property = property;
        this.message=message;
    }

    private String property;
    private String message;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
