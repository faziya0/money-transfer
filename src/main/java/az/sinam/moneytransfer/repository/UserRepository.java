package az.sinam.moneytransfer.repository;

import az.sinam.moneytransfer.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByPin(String pin);
}
