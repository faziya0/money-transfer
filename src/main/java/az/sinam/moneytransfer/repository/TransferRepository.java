package az.sinam.moneytransfer.repository;

import az.sinam.moneytransfer.entity.Account;
import az.sinam.moneytransfer.entity.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransferRepository extends JpaRepository<Transfer, Long> {

    @Query("SELECT t FROM Transfer t WHERE t.receiverAccount IN ?1 OR t.senderAccount IN ?1")
    List<Transfer> findByReceiverAccountInOrSenderAccountIn(List<Account> accounts);

    List<Transfer> findByReceiverAccountIn(List<Account> accounts);

    List<Transfer> findBySenderAccountIn(List<Account> accounts);

}
