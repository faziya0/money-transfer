package az.sinam.moneytransfer.repository;

import az.sinam.moneytransfer.entity.Account;
import az.sinam.moneytransfer.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findByUser(User user);

    List<Account> findByUserAndStatusIsTrue(User user);


    Optional<Account> findByIban(String iban);

    Optional<Account> findByIbanAndStatusIsTrue(String iban);

}
