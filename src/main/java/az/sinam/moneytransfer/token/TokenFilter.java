package az.sinam.moneytransfer.token;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TokenFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorization = request.getHeader("Authorization");
        try {
            if (authorization != null && authorization.startsWith("Bearer ")) {
                String token = authorization.substring(7);
                if (Boolean.TRUE.equals(jwtUtil.validateToken(token))) {
                    String usernameFromToken = jwtUtil.getUsernameFromToken(token);
                    List<Object> authorities = jwtUtil.getAuthorities(token);

                    Principal principal = () -> usernameFromToken;

                    List<GrantedAuthority> authorityList = new ArrayList<>();
                    for (Object authority : authorities) {
                        authorityList.add(new SimpleGrantedAuthority(authority.toString()));

                    }

                    UsernamePasswordAuthenticationToken auth =
                            new UsernamePasswordAuthenticationToken(principal, null, authorityList);

                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            }
        } catch (JwtException e) {
            Map<String, String> errorMessage = new HashMap<>();
            errorMessage.put("timestamp", LocalDateTime.now().toString());
            errorMessage.put("status", "401");
            errorMessage.put("message", "not valid token");
            errorMessage.put("path", request.getServletPath());
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.getWriter().write(convertObjectToJson(errorMessage));
            return;
        }
        filterChain.doFilter(request, response);
    }

    private String convertObjectToJson(Map<String,String> map) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(map);
    }

}


