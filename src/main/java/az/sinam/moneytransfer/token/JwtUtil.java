package az.sinam.moneytransfer.token;

import az.sinam.moneytransfer.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class JwtUtil {

    public JwtUtil(Key key) {
        this.key = key;
    }

    private final Key key;

    public String generateToken(String subject, Map<String, Object> claims) {
        return Jwts.builder().addClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis())).
                setExpiration(new Date(System.currentTimeMillis() + 15 * 60 * 1000)).signWith(key).compact();
    }

    public String createToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        Set<String> userRoles = new HashSet<>();
        for (GrantedAuthority authority : user.getAuthorities()) {
            userRoles.add(authority.getAuthority());
        }
        claims.put("roles", userRoles.toArray());
        return generateToken(user.getUsername(), claims);
    }

    private Claims getAllClaimsFromToken(String token) {

        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();

    }

    public String getUsernameFromToken(String token) {
        Claims allClaimsFromToken = getAllClaimsFromToken(token);
        return allClaimsFromToken.getSubject();

    }

    public Date getExpirationDateFromToken(String token) {
        Claims allClaimsFromToken = getAllClaimsFromToken(token);
        return allClaimsFromToken.getExpiration();
    }

    private Boolean isTokenExpired(String token) {
        Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public Boolean validateToken(String token) {
        String usernameFromToken = getUsernameFromToken(token);
        return usernameFromToken != null && !isTokenExpired(token);
    }

    public List<Object> getAuthorities(String token) {
        Claims allClaimsFromToken = getAllClaimsFromToken(token);
        return (List<Object>) allClaimsFromToken.get("roles", Object.class);
    }

}
