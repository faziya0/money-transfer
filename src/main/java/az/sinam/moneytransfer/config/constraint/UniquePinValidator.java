package az.sinam.moneytransfer.config.constraint;

import az.sinam.moneytransfer.entity.User;
import az.sinam.moneytransfer.repository.UserRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Optional;


public class UniquePinValidator implements ConstraintValidator<Unique, String> {
    public UniquePinValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private final UserRepository userRepository;

    @Override
    public boolean isValid(String pin, ConstraintValidatorContext context) {
        Optional<User> user = userRepository.findByPin(pin);
        return user.isEmpty();
    }


}
