package az.sinam.moneytransfer.config.constraint;


import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.math.BigDecimal;

public class CurrencyFormatValidator implements ConstraintValidator<CurrencyFormat, BigDecimal> {

    @Override
    public void initialize(CurrencyFormat constraintAnnotation) {
    }

    @Override
    public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
        if (value == null) {
            return true; // Consider using @NotNull for null checks
        }

        return (value.scale() == 2 || value.scale()==1) || value.scale()==0;
    }
}
