package az.sinam.moneytransfer.dto.response;

import java.math.BigDecimal;

public class AccountResponse {

    private Long id;
    private String accountType;
    private boolean status;
    private BigDecimal balance;
    private String iban;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }


    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", accountType='" + accountType + '\'' +
                ", status=" + status +
                ", balance=" + balance +
                ", iban='" + iban + '\'' +
                '}';
    }
}
