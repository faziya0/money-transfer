package az.sinam.moneytransfer.dto.response;

import java.math.BigDecimal;

public class TransferResponse {

    private Long id;
    private BigDecimal amount;
    private String senderMessage;
    private String senderAccountIban;
    private String receiverAccountIban;

    public TransferResponse(Long id, BigDecimal amount, String senderMessage, String senderAccountIban, String receiverAccountIban) {
        this.id = id;
        this.amount = amount;
        this.senderMessage = senderMessage;
        this.senderAccountIban = senderAccountIban;
        this.receiverAccountIban = receiverAccountIban;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getSenderMessage() {
        return senderMessage;
    }

    public void setSenderMessage(String senderMessage) {
        this.senderMessage = senderMessage;
    }

    public String getSenderAccountIban() {
        return senderAccountIban;
    }

    public void setSenderAccountIban(String senderAccountIban) {
        this.senderAccountIban = senderAccountIban;
    }

    public String getReceiverAccountIban() {
        return receiverAccountIban;
    }

    public void setReceiverAccountIban(String receiverAccountIban) {
        this.receiverAccountIban = receiverAccountIban;
    }

}
