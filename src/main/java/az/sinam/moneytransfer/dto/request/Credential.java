package az.sinam.moneytransfer.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

public class Credential {

    @NotBlank
    @Schema(defaultValue = "5645RTD")
    private String pin;

    @NotBlank
    @Schema(defaultValue = "u5g7ZqzY&r0EP45")
    private String password;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Credential(String pin, String password) {
        this.pin = pin;
        this.password = password;
    }

    public Credential() {
    }

}
