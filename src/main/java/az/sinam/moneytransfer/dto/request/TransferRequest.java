package az.sinam.moneytransfer.dto.request;

import az.sinam.moneytransfer.config.constraint.CurrencyFormat;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;


import java.math.BigDecimal;

public class TransferRequest {

    @NotBlank
    private String senderAccountIban;

    @NotBlank
    private String receiverAccountIban;

    @NotNull
    @CurrencyFormat
    private BigDecimal amount;

    @NotBlank
    private String senderMessage;

    public String getSenderAccountIban() {
        return senderAccountIban;
    }

    public void setSenderAccountIban(String senderAccountIban) {
        this.senderAccountIban = senderAccountIban;
    }

    public String getReceiverAccountIban() {
        return receiverAccountIban;
    }

    public void setReceiverAccountIban(String receiverAccountIban) {
        this.receiverAccountIban = receiverAccountIban;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getSenderMessage() {
        return senderMessage;
    }

    public void setSenderMessage(String senderMessage) {
        this.senderMessage = senderMessage;
    }

}
