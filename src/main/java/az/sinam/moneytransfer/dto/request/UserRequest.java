package az.sinam.moneytransfer.dto.request;

import az.sinam.moneytransfer.config.constraint.Unique;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class UserRequest {

    @NotBlank
    @Size(min = 2, max = 255)
    @Schema(description = "name of user", defaultValue = "testName")
    private String name;

    @NotBlank
    @Size(min = 4, max = 255)
    @Schema(description = "surname of user", defaultValue = "testSurname")
    private String surname;

    @NotBlank
    @Unique
    @Size(min = 7, max = 7, message = "pin code length must be 7")
    @Schema(description = "pin of user", defaultValue = "5645RTD")
    private String pin;

    @NotBlank
    @Size(min = 8, max = 255)
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*$", message = "Password must have at least 1 uppercase, 1 lowercase letter and 1 number")
    @Schema(description = "password of user", defaultValue = "u5g7ZqzY&r0EP45")
    private String password;

    @NotBlank
    @Schema(description = "email of user", defaultValue = "test@email.com")
    private String email;

    public UserRequest() {
    }

    public UserRequest(String name, String surname, String pin, String password, String email) {
        this.name = name;
        this.surname = surname;
        this.pin = pin;
        this.password = password;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
