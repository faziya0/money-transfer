package az.sinam.moneytransfer.controller;

import az.sinam.moneytransfer.dto.request.AccountRequest;
import az.sinam.moneytransfer.dto.response.AccountResponse;
import az.sinam.moneytransfer.service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    @Operation(summary = "getAllAccount")
    public List<AccountResponse> getAllAccount() {
        return accountService.getAllAccount();
    }

    @GetMapping("/{iban}")
    @PreAuthorize("@accountServiceImpl.isAccountOwner(#iban)")
    @Operation(summary = "getAccountByIban")
    public AccountResponse getAccountByIban(@PathVariable String iban) {
        return accountService.getAccountByIban(iban);
    }

    @PostMapping
    @Operation(summary = "createAccount")
    public AccountResponse createAccount(@Valid @RequestBody AccountRequest accountRequest) {
        return accountService.createAccount(accountRequest);
    }

    @PreAuthorize("@accountServiceImpl.isAccountOwner(#iban)")
    @DeleteMapping("/{iban}")
    @Operation(summary = "deleteAccount")
    public void deleteAccount(@PathVariable String iban) {
        accountService.deleteAccount(iban);
    }

    @PreAuthorize("@accountServiceImpl.isAccountOwner(#iban)")
    @PutMapping("/{iban}")
    @Operation(summary = "activateAccount")
    public AccountResponse activateAccount(@PathVariable String iban) {
        return accountService.activateAccount(iban);
    }

    @GetMapping("/active/{pinCode}")
    @Operation(summary = "getAllActiveAccountByUserPin")
    public List<AccountResponse> getAllActiveAccountByUserPin(@PathVariable String pinCode) {
        return accountService.getAllActiveAccountByUserPin(pinCode);
    }

}
