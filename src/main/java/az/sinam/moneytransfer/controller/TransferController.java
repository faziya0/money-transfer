package az.sinam.moneytransfer.controller;

import az.sinam.moneytransfer.dto.request.TransferRequest;
import az.sinam.moneytransfer.dto.response.TransferResponse;
import az.sinam.moneytransfer.service.TransferService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/transfers")
public class TransferController {

    private final TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }


    @PostMapping
    @PreAuthorize("@accountServiceImpl.isAccountOwner(#transferRequest)")
    @Operation(summary = "transfer")
    public TransferResponse transfer(@Valid @RequestBody TransferRequest transferRequest) {
        return transferService.transfer(transferRequest);
    }

    @GetMapping
    @Operation(summary = "getAllTransfer")
    public List<TransferResponse> getAllTransfer() {
        return transferService.getAllTransfer();
    }

    @GetMapping("/sender")
    @Operation(summary = "getAllTransferFromSender")
    public List<TransferResponse> getAllTransferFromSender() {
        return transferService.getAllTransferFromSender();
    }

    @GetMapping("/receiver")
    @Operation(summary = "getAllTransferFromReceiver")
    public List<TransferResponse> getAllTransferFromReceiver() {
        return transferService.getAllTransferFromReceiver();
    }

}
