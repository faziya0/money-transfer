package az.sinam.moneytransfer.controller;

import az.sinam.moneytransfer.dto.request.UserRequest;
import az.sinam.moneytransfer.dto.response.UserResponse;
import az.sinam.moneytransfer.entity.User;
import az.sinam.moneytransfer.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "createUser")
    public UserResponse createUser(@Valid @RequestBody UserRequest userRequest) {
        return userService.createUser(userRequest);
    }

    @GetMapping("/{pin}")
    @Operation(summary = "getUser")
    public UserResponse getUser(@PathVariable String pin) {
        User user = userService.findUserByPin(pin);
        return modelMapper.map(user, UserResponse.class);
    }

}
