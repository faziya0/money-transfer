package az.sinam.moneytransfer.controller;

import az.sinam.moneytransfer.service.AuthService;
import az.sinam.moneytransfer.dto.request.Credential;
import az.sinam.moneytransfer.dto.response.AuthResponse;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    @Operation(summary = "authenticate")
    public AuthResponse authenticate(@Valid @RequestBody Credential credential) {
        return authService.auth(credential);
    }

}

