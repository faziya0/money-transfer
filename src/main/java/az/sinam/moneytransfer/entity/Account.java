package az.sinam.moneytransfer.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Entity
public class Account implements Serializable {

    @Serial
    private static final long serialVersionUID = 2405172041950251807L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean status;

    @Column(unique = true)
    private String iban;

    private BigDecimal balance;


    private String accountType;

    @OneToMany(mappedBy = "receiverAccount", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Transfer> receiverTransfers;

    @OneToMany(mappedBy = "senderAccount", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Transfer> senderTransfers;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAccountType() {
        return accountType;
    }


    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @PrePersist
    public void setUp() {
        balance = new BigDecimal(300.00);
        status = Boolean.FALSE;
        iban = UUID.randomUUID().toString().toUpperCase();
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;

    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public List<Transfer> getReceiverTransfers() {
        return receiverTransfers;
    }

    public void setReceiverTransfers(List<Transfer> receiverTransfers) {
        this.receiverTransfers = receiverTransfers;
    }

    public List<Transfer> getSenderTransfers() {
        return senderTransfers;
    }

    public void setSenderTransfers(List<Transfer> senderTransfers) {
        this.senderTransfers = senderTransfers;
    }
}

